
    package serverSideLauncher;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

//import org.apache.commons.io.FileUtils;


public class ServerSideLauncher {
	private static String exportFolder = "..\\export\\";
	private static String importFolder = ".\\";
	private static List gameFolder =  new ArrayList<>();
	public static void main(String[] args) throws FileNotFoundException, IOException {
	  init();
	  checkFolder();
	}
	
	@SuppressWarnings("unchecked")
	private static void init(){
		gameFolder.add("Animations");
		gameFolder.add("ForceFeedback");
		gameFolder.add("L2text"); 
		gameFolder.add("MAPS"); 
		gameFolder.add("music"); 
		gameFolder.add("Sounds");
		gameFolder.add("StaticMeshes");
		gameFolder.add("system");
		gameFolder.add("SysTextures");
		gameFolder.add("Textures");
		gameFolder.add("Voice");
	}
	/**
	 * Explore and replicate the structure of the game , make an md5 of the files and zip them to the export folder precised in args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static void checkFolder() throws FileNotFoundException, IOException{
		cloneFolder(importFolder, exportFolder);
		gameFolder.parallelStream().forEach((s)->{
			Collection<File> all = new ArrayList<File>();
			Map<String, String> md5map = new HashMap<>();
			addTree(new File(importFolder+s), all);
			System.out.println(all);
			all.forEach((a)->{
					try {
						System.out.println("Calculating md5 for : " + a.getPath());
						String result = calcMd5(a);
						if(a != null){
							md5map.put(a.getPath(), result);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
			});
			md5map.forEach((a,b)->{
				try {
					Writer output = null;
					String text = a + ":"+b+"\r";
					output = new BufferedWriter(new FileWriter("hash.txt", true));
					output.write(text);
					output.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			});
			all.forEach((a)->{
				System.out.println("Ziping : "+a.getName());
				zip(a.getName(),a.getPath());
			});
			all.clear();
			md5map.clear();
		});
	}
	
	/**
	 * explore recursivly the folder tree and insert the files name in the map
	 * @param file
	 * @param all
	 */
	public static void addTree(File file, Collection<File> all) {
	    File[] children = file.listFiles();
	    if (children != null) {
	        for (File child : children) {
	            all.add(child);
	            addTree(child, all);
	        }
	    }
	}
	
	/**
	 * calc the md5
	 * @param a
	 * @return String the md5 string
	 * @throws IOException
	 */
	public static String calcMd5(File a) throws IOException{
		FileInputStream fil = new FileInputStream(a);
		String md5s = org.apache.commons.codec.digest.DigestUtils.md5Hex(fil);
		fil.close();
		return md5s;
	}
	
	public static void zip(String fileName,String filePathName){
		try{
			byte[] buffer = new byte[1024];
			System.out.println(exportFolder+filePathName.substring(2) + ".zip");
    		FileOutputStream fos = new FileOutputStream(exportFolder+filePathName.substring(2) + ".zip");
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze = new ZipEntry(fileName);
    		zos.putNextEntry(ze);
    		System.out.println(importFolder + filePathName.substring(2));
    		FileInputStream in = new FileInputStream( importFolder + filePathName.substring(2));

    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();

    		//remember close it
    		zos.close();
    		
    		System.out.println(fileName + " : Done");

    	}catch(IOException ex){
    	   ex.printStackTrace();
    	}
	}

	public static String getExportFolder() {
		return exportFolder;
	}

	public static void setExportFolder(String exportFolder) {
		ServerSideLauncher.exportFolder = exportFolder;
	}
	
	/**
	 * clone le dossier et son arborescence dans le dossier cible
	 * @param source
	 * @param target
	 */
	public static void cloneFolder(String source, String target) {
        File targetFile = new File(target);
        if (!targetFile.exists()) {
            targetFile.mkdir();
        }
        for (File f : new File(source).listFiles()) {
        if (f.isDirectory()) {
                String append = "/" + f.getName();
                System.out.println("Creating '" + target + append + "': "
                        + new File(target + append).mkdir());
                cloneFolder(source + append, target + append);
            }
        }
    }   
	
}


